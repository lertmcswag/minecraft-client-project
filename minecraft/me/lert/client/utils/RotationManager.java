package me.lert.client.utils;

import me.lert.client.base.Wrapper;

public class RotationManager {
	/**
	 * @author DarkMagician6
	 * 
	 */

	private float pitch;
	private float yaw;
	private float oldPitch;
	private float oldYaw;

	public float getYaw() {
		return this.yaw;
	}

	public float getPitch() {
		return this.pitch;
	}

	public float getOldYaw() {
		return this.oldYaw;
	}

	public float getOldPitch() {
		return this.oldPitch;
	}

	public void setPitch(float var1) {
		this.pitch = var1;
	}

	public void setYaw(float var1) {
		this.yaw = var1;
	}

	public void setOldPitch(float var1) {
		this.oldPitch = var1;
	}

	public void setOldYaw(float var1) {
		this.oldYaw = var1;
	}

	public void preUpdate() {
		this.yaw = Wrapper.getPlayer().rotationYaw;
		this.pitch = Wrapper.getPlayer().rotationPitch;
	}

	public void postUpdate() {
		this.oldPitch = this.pitch;
		this.oldYaw = this.yaw;
	}
}