package me.lert.client.utils;

public class Colors {
	/**
	 * @author EmotionalPatrick
	 * 
	 */
	public static String Black = "\2470";
	public static String Dark_Blue = "\2471";
	public static String Dark_Green = "\2472";
	public static String Dark_aqua = "\2473";
	public static String Dark_Red = "\2474";
	public static String Dard_Purple = "\2475";
	public static String Gold = "\2476";
	public static String Grey = "\2477";
	public static String Dark_Grey = "\2478";
	public static String Blue = "\2479";
	public static String Green = "\247a";
	public static String Aqua = "\247b";
	public static String Red = "\247c";
	public static String Light_Purple = "\247d";
	public static String Yellow = "\247e";
	public static String White = "\247f";
	public static String Random = "\247k";
	public static String Bold = "\247l";
	public static String Strikethrough = "\247m";
	public static String Underline = "\247n";
	public static String Italic = "\247o";
	public static String Reset = "\247r";
}