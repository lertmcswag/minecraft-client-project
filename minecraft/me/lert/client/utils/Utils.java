package me.lert.client.utils;

import java.awt.Color;
import java.util.Random;

public class Utils {

	/** @author Halalaboos **/
	public Color getRandomColor() {
		Random var1 = new Random();
		float var2 = (float) (var1.nextInt(255) + 1) / 255.0F;
		float var3 = (float) (var1.nextInt(255) + 1) / 255.0F;
		float var4 = (float) (var1.nextInt(255) + 1) / 255.0F;
		return new Color(var2, var3, var4);
	}
}