package me.lert.client.screens;

import me.lert.client.base.Client;
import me.lert.client.base.Wrapper;
import me.lert.client.modules.Module;
import net.minecraft.src.FontRenderer;

public class InGameGui {

	public static void renderOverlay() {
		renderName();
		renderMods();
	}

	private static void renderName() {
		FontRenderer fr = Wrapper.getFontRenderer();
		fr.drawStringWithShadow(
				Client.getClientName() + Client.getClientVersion(), 2, 2,
				0xFFFFFF);
	}

	private static void renderMods() {
		FontRenderer fr = Wrapper.getFontRenderer();
		int yPos = 2;
		for (Module m : Client.getManager().getMods()) {
			if (m.getState() && m.shouldRender()) {
				String name = m.getName();
				int xPos = Wrapper.getScaledResolution().getScaledWidth()
						- (Wrapper.getFontRenderer().getStringWidth(name) + 2);
				int color = m.getColor();
				fr.drawStringWithShadow(name, xPos, yPos, color);
				yPos += 10;
			}
		}
	}
}