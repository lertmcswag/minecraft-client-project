package me.lert.client.modules;

import me.lert.client.base.Wrapper;

public class Fly extends Module {

	public Fly() {
		super("Fly", "Allows the player to fly.", "F", true);
	}

	@Override
	public void onEnable() {
		if (isNotCreative()) {
			Wrapper.getPlayer().capabilities.allowFlying = true;
		}
	}

	@Override
	public void onDisable() {
		if (isNotCreative()) {
			Wrapper.getPlayer().capabilities.allowFlying = false;
			Wrapper.getPlayer().capabilities.isFlying = false;
		}
	}

	private boolean isNotCreative() {
		return !Wrapper.getPlayer().capabilities.isCreativeMode;
	}
}