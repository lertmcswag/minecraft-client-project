package me.lert.client.modules;

import me.lert.client.base.Wrapper;

public class Module {

	private String key;
	private int color = Wrapper.getUtils().getRandomColor().getRGB();
	private String name;
	private String desc;
	private boolean enabled;
	private boolean render;

	public Module(String name, String desc, String key, boolean render) {
		this.name = name;
		this.desc = desc;
		this.key = key;
		this.render = render;
	}

	public Module(String name, String desc, String key, boolean render,
			boolean enabled) {
		this.name = name;
		this.desc = desc;
		this.key = key;
		this.render = render;
		this.enabled = enabled;
	}

	/** Called when Module is enabled **/
	public void onEnable() {

	}

	/** Called when Module is disabled **/
	public void onDisable() {
	}

	/** Called on pre motion updates **/
	public void onPreMotion() {

	}

	/** Called every game tick **/
	public void onTick() {
	}

	/** Called on post motion updates **/
	public void onPostMotion() {

	}

	/** Called when the player respawns **/
	public void onDeath() {

	}

	/** Called when gui is rendered **/
	public void onRenderGui() {

	}

	/** Sets the Module on/off **/
	public void setToggle(boolean b) {
		this.enabled = b;
		if (this.getState()) {
			this.onEnable();
		} else {
			this.onDisable();
		}
	}

	/** Used to toggle the Module **/
	public void onToggle() {
		this.color = Wrapper.getUtils().getRandomColor().getRGB();
		enabled = !enabled;
		if (this.getState()) {
			this.onEnable();
		} else {
			this.onDisable();
		}
	}

	/** Returns with the Modules state **/
	public boolean getState() {
		return this.enabled;
	}

	/** Returns if the Module should render on Gui **/
	public boolean shouldRender() {
		return this.render;
	}

	public int getColor() {
		return this.color;
	}

	public String getKey() {
		return this.key;
	}

	public String getName() {
		return this.name;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setColor(int c) {
		this.color = c;
	}

	public void setKey(String k) {
		this.key = k;
	}

	public void setName(String n) {
		this.name = n;
	}

	public void setDesc(String d) {
		this.desc = d;
	}
}