package me.lert.client.modules;

import java.util.ArrayList;

import me.lert.client.base.Wrapper;

public class Xray extends Module {

	private static ArrayList<Integer> blockID = new ArrayList<Integer>();
	private static int opacity = 135;

	public Xray() {
		super("Xray", "Xrays the world to show ores", "X", true);
	}

	@Override
	public void onEnable() {
		Wrapper.getGlobal().loadRenderers();
		blockID.add(8);
		blockID.add(9);
		blockID.add(10);
		blockID.add(11);
		blockID.add(14);
		blockID.add(15);
		blockID.add(16);
		blockID.add(56);
	}

	@Override
	public void onDisable() {
		Wrapper.getGlobal().loadRenderers();
	}

	public static boolean isXrayable(int block) {
		return blockID.contains(block);
	}
	
	public static int getOpacity() {
		return opacity;
	}
}