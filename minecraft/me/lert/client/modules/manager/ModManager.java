package me.lert.client.modules.manager;

import java.util.ArrayList;

import me.lert.client.modules.Fly;
import me.lert.client.modules.Module;
import me.lert.client.modules.Respawn;
import me.lert.client.modules.Sprint;
import me.lert.client.modules.Step;

public class ModManager {

	private ArrayList<Module> mods = new ArrayList<Module>();

	public ModManager() {
		mods.add(new Fly());
		mods.add(new Respawn());
		mods.add(new Sprint());
		mods.add(new Step());
	}

	public ArrayList<Module> getMods() {
		return this.mods;
	}

	public Module getModName(String n) {
		for (Module m : getMods()) {
			if (m.getName().equalsIgnoreCase(n)) {
				return m;
			}
		}
		return null;
	}
}