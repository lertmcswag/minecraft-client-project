package me.lert.client.modules.manager;

import java.util.ArrayList;

import me.lert.client.modules.Fly;
import me.lert.client.modules.Module;
import me.lert.client.modules.Respawn;
import me.lert.client.modules.Sprint;
import me.lert.client.modules.Step;
import me.lert.client.modules.Xray;

public class ModuleManager {

	private ArrayList<Module> mods = new ArrayList<Module>();

	public ModuleManager() {
		mods.add(new Fly());
		mods.add(new Respawn());
		mods.add(new Sprint());
		mods.add(new Step());
		mods.add(new Xray());
	}

	public ArrayList<Module> getMods() {
		return this.mods;
	}

	public Module getMod(String n) {
		for (Module m : getMods()) {
			if (m.getName().equalsIgnoreCase(n)) {
				return m;
			}
		}
		return null;
	}
}