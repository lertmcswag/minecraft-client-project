package me.lert.client.modules;

import me.lert.client.base.Client;
import me.lert.client.base.Helper;
import me.lert.client.base.Wrapper;
import net.minecraft.src.Packet11PlayerPosition;

public class Step extends Module {

	public Step() {
		super("Step", "Steps a up block.", "NONE", true);
	}
	
	@Override
	public void onPreMotion() {
		double x = Wrapper.getPlayer().posX;
		double y = Wrapper.getPlayer().posY + 1;
		double z = Wrapper.getPlayer().posZ;
		if (stepChecks()) {
			Wrapper.getPlayer().setPosition(x, y, z);
			if (Client.getManager().getMod("Sprint").getState()) {
				Wrapper.getPlayer().setSprinting(true);
			}
		}
	}

	private boolean stepChecks() {
		return Wrapper.getPlayer().isCollidedHorizontally
				&& Wrapper.getPlayer().onGround
				&& !Wrapper.getPlayer().isOnLadder()
				&& !Wrapper.getPlayer().isInWater()
				&& Wrapper.getPlayer().moveForward >= 0.5;
	}
}