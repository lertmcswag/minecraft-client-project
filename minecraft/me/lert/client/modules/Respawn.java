package me.lert.client.modules;

import me.lert.client.base.Wrapper;

public class Respawn extends Module{

	public Respawn() {
		super("Respawn", "Respawns the player on death.", "NONE", false, true);
	}

	@Override
	public void onDeath() {
		Wrapper.getPlayer().respawnPlayer();
	}
}
