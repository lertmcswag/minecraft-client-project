package me.lert.client.modules.hooks;

import me.lert.client.base.Client;
import me.lert.client.modules.Module;
import me.lert.client.modules.manager.ModuleManager;

import org.lwjgl.input.Keyboard;

public class Hooks {

	/**
	 * Hook set at line 74, in EntityClientPlayerMP.java Called before motion
	 * updates
	 */
	public void onPreMotionUpdate() {
		for (Module m : Client.getManager().getMods()) {
			if (m.getState()) {
				m.onPreMotion();
			}
		}
	}

	/**
	 * Hook set at line ???, in EntityClientPlayerMP.java Called on every game
	 * tick
	 */
	public void onGameTick() {
		for (Module m : Client.getManager().getMods()) {
			if (m.getState()) {
				m.onTick();
			}
		}
	}

	/**
	 * Hook set at line 154, in EntityClientPlayerMP.java Called after motion
	 * updates
	 */
	public void onPostMotionUpdate() {
		for (Module m : Client.getManager().getMods()) {
			if (m.getState()) {
				m.onPostMotion();
			}
		}
	}

	/**
	 * Hook set at line 588, in Minecraft.java Called when player dies
	 */
	public void onPlayerDeath() {
		for (Module p : Client.getManager().getMods()) {
			if (p.getState()) {
				p.onDeath();
			}
		}
	}

	/**
	 * Hook set at line 1562, in Minecraft.java Called, once the key is pressed.
	 **/
	public void onKeyPressed(int key) {
		for (Module mod : Client.getManager().getMods()) {
			if ((Keyboard.getKeyIndex(mod.getKey()) == key) && (!mod.getKey().equalsIgnoreCase("NONE"))) {
				mod.onToggle();
			}
		}
	}

	/**
	 * Hook set at line 317, in Minecraft.java Called when in game gui is
	 * rendered
	 **/
	public void onGuiRender() {
		for (Module p : Client.getManager().getMods()) {
			if (p.getState()) {
				p.onRenderGui();
			}
		}
	}
}