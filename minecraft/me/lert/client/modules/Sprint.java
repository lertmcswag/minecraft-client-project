package me.lert.client.modules;

import me.lert.client.base.Helper;
import me.lert.client.base.Wrapper;
import net.minecraft.src.FontRenderer;

import org.lwjgl.input.Keyboard;

public class Sprint extends Module {

	public Sprint() {
		super("Sprint", "Player sprints on foward movement.", "V", true);
	}

	@Override
	public void onPreMotion() {
		if (shouldSprint()) {
			Wrapper.getPlayer().setSprinting(true);
		}
	}

	private boolean shouldSprint() {
		return Wrapper.getPlayer().moveForward >= 0.5
				&& Wrapper.getPlayer().getFoodStats().getFoodLevel() >= 6;
	}
}