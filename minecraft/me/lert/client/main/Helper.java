package me.lert.client.main;

import net.minecraft.src.Packet;

public class Helper {

	public static void addChat(String s) {
		Wrapper.getPlayer().addChatMessage(s);
	}

	public static void sendChat(String s) {
		Wrapper.getPlayer().sendChatMessage(s);
	}

	public static void sendPacket(Packet p) {
		Wrapper.getMinecraft().getNetHandler().addToSendQueue(p);
	}

	public static void addConsole(String s) {
		System.out.println("[Client] " + s);
	}
}