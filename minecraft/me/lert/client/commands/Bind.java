package me.lert.client.commands;

import me.lert.client.base.Client;
import me.lert.client.base.Helper;
import me.lert.client.modules.Module;

public class Bind extends Command {

	public Bind() {
		super(".bind", "Rebinds a module to a key.", ".bind (module) (key)");
	}

	public void runCommand(String args, String[] var) {
		try {
			for (Module m : Client.getManager().getMods()) {
				if (var[1].equalsIgnoreCase(m.getName())) {
					m.setKey(var[2].toUpperCase());
					Helper.addChat(m.getName() + " set to " + var[2].toUpperCase());
				}
			}
		} catch (Exception e) {
			Helper.addChat(this.getSyntax());
		}
	}
}