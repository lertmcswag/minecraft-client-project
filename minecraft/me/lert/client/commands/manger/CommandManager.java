package me.lert.client.commands.manger;

import java.util.ArrayList;

import me.lert.client.commands.Bind;
import me.lert.client.commands.Command;
import me.lert.client.commands.Gamemode;
import me.lert.client.commands.Help;
import me.lert.client.commands.Toggle;

public class CommandManager {

	private ArrayList<Command> commands = new ArrayList<Command>();

	public CommandManager() {
		commands.add(new Bind());
		commands.add(new Gamemode());
		commands.add(new Help());
		commands.add(new Toggle());
	}

	public void runCommands(String args, String[] var) {
		for (Command c : commands) {
			if (args.startsWith(c.getCommand())) {
				c.runCommand(args, var);
			}
		}
	}

	public ArrayList<Command> getCommands() {
		return this.commands;
	}

	public Command getCommandName(String c) {
		for (Command cmd : getCommands()) {
			if (cmd.getCommand().equalsIgnoreCase(c)) {
				return cmd;
			}
		}
		return null;
	}
}