package me.lert.client.commands;

import me.lert.client.main.Helper;

public class Test extends Command {

	public Test() {
		super(".test", "Test command", ".help");
	}

	@Override
	public void runCommand(String args, String[] var) {
		try {
			if (var[0].equalsIgnoreCase("works")) {
				Helper.addChat("Success");
			}
		} catch (Exception e) {
			this.getSyntax();
		}
	}
}