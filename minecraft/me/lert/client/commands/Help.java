package me.lert.client.commands;

import me.lert.client.base.Client;
import me.lert.client.base.Helper;
import me.lert.client.modules.Module;

public class Help extends Command {

	public Help() {
		super(".help", "Sends module, or command description.",
				".help (module/command)");
	}

	public void runCommand(String args, String[] var) {
		try {
			if (var[1].equalsIgnoreCase("module")) {
				for (Module m : Client.getManager().getMods()) {
					Helper.addChat("Modules: " + m.getName());
				}
			}
			if (var[1].equalsIgnoreCase("command")) {
				for (Command c : Client.getCommandManager().getCommands()) {
					Helper.addChat("Commands: " + c.getCommand());
				}
			}
		} catch (Exception e) {
			Helper.addChat(this.getSyntax());
		}
	}
}