package me.lert.client.commands;

import me.lert.client.base.Helper;
import me.lert.client.base.Wrapper;

public class Gamemode extends Command {

	public Gamemode() {
		super(".gm", "Changes player gamemode between creative/survival", ".gm");
	}

	public void runCommand(String args, String[] var) {
		Helper.sendChat("/gamemode "
				+ (this.isCreative() ? "survival" : "creative"));
	}

	private boolean isCreative() {
		return Wrapper.getPlayer().capabilities.isCreativeMode;
	}
}