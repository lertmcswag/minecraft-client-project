package me.lert.client.commands;

import me.lert.client.base.Client;
import me.lert.client.base.Helper;
import me.lert.client.modules.Module;

public class Toggle extends Command {

	public Toggle() {
		super(".t", "Toggles module", ".t (module)");
	}

	public void runCommand(String args, String[] var) {
		try {
			for (Module m : Client.getManager().getMods()) {
				if (var[1].equalsIgnoreCase(m.getName())) {
					Helper.addChat(m.getName() + " toggled "
							+ (m.getState() ? "on." : "off."));
					m.onToggle();
				}
			}
		} catch (Exception e) {
			Helper.addChat(this.getSyntax());
		}
	}
}