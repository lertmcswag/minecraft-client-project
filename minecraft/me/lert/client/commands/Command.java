package me.lert.client.commands;

public abstract class Command {

	private String command;
	private String desc;
	private String syntax;

	public Command(String command, String desc, String syntax) {
		this.command = command;
		this.desc = desc;
		this.syntax = syntax;
	}

	/** Runs the command **/
	public abstract void runCommand(String args, String[] var);

	/** Returns with Commands name **/
	public String getCommand() {
		return this.command;
	}

	/** Returns with Commands description **/
	public String getDesc() {
		return this.desc;
	}

	/** Returns Commands syntax **/
	public String getSyntax() {
		return "Correct syntax: " + this.syntax;
	}
}