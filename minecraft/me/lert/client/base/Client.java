package me.lert.client.base;

import me.lert.client.commands.manger.CommandManager;
import me.lert.client.modules.hooks.Hooks;
import me.lert.client.modules.manager.ModuleManager;
import me.lert.client.utils.RotationManager;
import me.lert.client.utils.Utils;

public class Client {

	private static String clientName = "Client", clientVersion = " v1",
			minecraftVersion = "1.6.2";
	private static RotationManager rotationManager = new RotationManager();
	private static Utils clientUtils = new Utils();
	private static Hooks clientHooks = new Hooks();
	private static ModuleManager modManager = new ModuleManager();
	private static CommandManager commandManager = new CommandManager();

	public static void onStartUp() {
		Helper.addConsole(clientName + " initiated.");
	}

	public static RotationManager getRotation() {
		return rotationManager;
	}

	public static Utils getUtils() {
		return clientUtils;
	}

	public static Hooks getHooks() {
		return clientHooks;
	}

	public static CommandManager getCommandManager() {
		return commandManager ;
	}
	
	public static ModuleManager getManager() {
		return modManager;
	}

	public static String getClientName() {
		return clientName;
	}

	public static String getClientVersion() {
		return clientVersion;
	}

	public static String getMinecraftVersion() {
		return minecraftVersion;
	}
}