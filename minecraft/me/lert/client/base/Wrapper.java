package me.lert.client.base;

import me.lert.client.modules.hooks.Hooks;
import me.lert.client.utils.RotationManager;
import me.lert.client.utils.Utils;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EntityRenderer;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.Minecraft;
import net.minecraft.src.PlayerControllerMP;
import net.minecraft.src.RenderGlobal;
import net.minecraft.src.ScaledResolution;
import net.minecraft.src.WorldClient;

public class Wrapper {

	/**
	 * @author EmotionalPatrick
	 */

	/** Returns with Minecraft */
	public static Minecraft getMinecraft() {
		return Minecraft.getMinecraft();
	}

	/** Return the EntityRenderer */
	public static EntityRenderer getRenderer() {
		return getMinecraft().entityRenderer;
	}

	/** Returns the PlayerController */
	public static PlayerControllerMP getController() {
		return getMinecraft().playerController;
	}

	/** Returns the ScaledResolution */
	public static ScaledResolution getScaledResolution() {
		return new ScaledResolution(getMinecraft().gameSettings,
				getMinecraft().displayWidth, getMinecraft().displayHeight);
	}

	/** Returns the Screen Width */
	public static int getScreenWidth() {
		ScaledResolution var0 = new ScaledResolution(
				getMinecraft().gameSettings, getMinecraft().displayWidth,
				getMinecraft().displayHeight);
		return var0.getScaledWidth();
	}

	/** Returns the Screen Height */
	public static int getScreenHeight() {
		ScaledResolution var0 = new ScaledResolution(
				getMinecraft().gameSettings, getMinecraft().displayWidth,
				getMinecraft().displayHeight);
		return var0.getScaledHeight();
	}

	/** Returns Minecraft's FontRenderer */
	public static FontRenderer getFontRenderer() {
		return getMinecraft().fontRenderer;
	}

	/** Return the player */
	public static EntityClientPlayerMP getPlayer() {
		return getMinecraft().thePlayer;
	}

	/** Return the world */
	public static WorldClient getWorld() {
		return getMinecraft().theWorld;
	}

	/** Return the RenderGlobal */
	public static RenderGlobal getGlobal() {
		return getMinecraft().renderGlobal;
	}

	/** Return client Utils **/
	public static Utils getUtils() {
		return Client.getUtils();
	}

	/** Returns DarkMagician6 Rotation Manager **/
	public static RotationManager getRotation() {
		return Client.getRotation();
	}

	/** Returns module Hooks **/
	public static Hooks getHooks() {
		return Client.getHooks();
	}
}